import "./assets/css/tailwind.css";

import { createApp } from "vue";
import GCurv from "./GCurv.vue";

import { createRouter, createWebHistory } from "vue-router";

import GcIcon from "./components/GCIcon.vue";
import Generic from "./views/Generic.vue"

createApp(GCurv)
  .component("gc-icon", GcIcon)
  .use(createRouter({
    history: createWebHistory(import.meta.env.BASE_URL),
    routes: [
      {
        path: "/",
        name: "Generic",
        props: true,
        component: Generic
      },
      {
        path: "/:pages(.*)",
        redirect: { name: "Generic" }
      }
    ],
  }))
  .mount('#app')